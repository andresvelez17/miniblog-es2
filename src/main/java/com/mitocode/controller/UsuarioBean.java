package com.mitocode.controller;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.mindrot.jbcrypt.BCrypt;

import com.mitocode.model.Persona;
import com.mitocode.model.Usuario;
import com.mitocode.service.IUsuarioService;

@Named
@ViewScoped
public class UsuarioBean implements Serializable {
	
	@Inject
	private IUsuarioService service;
	private Usuario usuario;
	private List<Usuario> lista;
	private String contrasenaActual;
	private String contrasenaValidada = "NO";
	private String valorBuscado;
	
	
	@PostConstruct
	public void init() {
		this.usuario = new Usuario();
		this.listar();
	}
	
	public void listar() {
		try {
			this.lista = this.service.listar();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void mostrarData(Usuario u) {
		this.usuario = u;
	}	
	
	public void verificarContrasena() {
		String claveGuardada = usuario.getContrasena();
       
		try {
			if (BCrypt.checkpw(contrasenaActual, claveGuardada)) {
	        	contrasenaValidada = "SI";
			}else {
	        	contrasenaValidada = "NO";			
	        }
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	
	public void limpiarControles() {
		this.usuario = new Usuario();
		this.contrasenaActual = "";
    	this.contrasenaValidada = "NO";	
	}
	
	
	public void modificar() {
		try {

			String clave = this.usuario.getContrasena();
			String claveHash = BCrypt.hashpw(clave, BCrypt.gensalt());
			this.usuario.setContrasena(claveHash);					
			this.service.modificar(this.usuario);	
        	this.contrasenaValidada = "NO";	
        	this.usuario = new Usuario();
        	this.contrasenaActual = "";
			this.listar();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void buscar() {
		try {			
			this.lista = this.service.listarPorNombreUsuario(valorBuscado);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public List<Usuario> getLista() {
		return lista;
	}

	public void setLista(List<Usuario> lista) {
		this.lista = lista;
	}

	public String getContrasenaActual() {
		return contrasenaActual;
	}

	public void setContrasenaActual(String contrasenaActual) {
		this.contrasenaActual = contrasenaActual;
	}

	public String getContrasenaValidada() {
		return contrasenaValidada;
	}

	public void setContrasenaValidada(String contrasenaValidada) {
		this.contrasenaValidada = contrasenaValidada;
	}

	public String getValorBuscado() {
		return valorBuscado;
	}

	public void setValorBuscado(String valorBuscado) {
		this.valorBuscado = valorBuscado;
	}	

}
